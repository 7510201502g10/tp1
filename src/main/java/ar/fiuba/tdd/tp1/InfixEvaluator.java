package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.enums.Operator;
import ar.fiuba.tdd.tp1.exception.InvalidFormulaException;
import ar.fiuba.tdd.tp1.operation.ICalculator;
import ar.fiuba.tdd.tp1.parser.ReferenceCellParser;
import ar.fiuba.tdd.tp1.util.Util;
import ar.fiuba.tdd.tp1.validator.FormulaValidator;
import ar.fiuba.tdd.tp1.validator.IValidator;

import java.util.Hashtable;
import java.util.Stack;

/**
 * Encargada de resolver la formula introducida en una celda.
 *
 */
public class InfixEvaluator {

    private Stack<String> operators;
    private Stack<String> operands;
    private Hashtable<String, ICalculator> hashOperators;
    private IValidator formulaValidator;

    public IValidator getFormulaValidator() {
        return formulaValidator;
    }

    public InfixEvaluator() {
        operators = new Stack<String>();
        operands = new Stack<String>();
        hashOperators = new Hashtable<String, ICalculator>();
        formulaValidator = new FormulaValidator();
    }

    public String infix(String expression, String bookName) throws InvalidFormulaException {

        this.initializeHash();
        Util util = new Util();
        this.formulaValidate(expression);
        expression = expression.replace(Constant.EQUAL + Constant.SPACE, "") + Constant.SPACE + Constant.EQUAL;
        for (String token : expression.split(" ")) {
            if (token != null && util.isNumeric(token)) {
                operands.push(token);
            } else if (Operator.isOperator(token)) {
                operators.push(token);
            } else {
                ReferenceCellParser rcp = new ReferenceCellParser(bookName);
                rcp.parserReferenceCell(token);
                operands.push(this.getValueCell(rcp));
            }
            resolve(operands, operators);
        }
        return (String) operands.pop();
    }

    private void resolve(Stack<String> operands, Stack<String> operators) {
        while (operators.size() >= 2) {
            String firstOperator = (String) operators.pop();
            String secondOperator = (String) operators.pop();
            if (Operator.getPriorityFromOperator(firstOperator) < Operator.getPriorityFromOperator(secondOperator)) {
                operators.push(secondOperator);
                operators.push(firstOperator);
                break;
            } else {
                String firstOperand = (String) operands.pop();
                String secondOperand = (String) operands.pop();
                operands.push(getResult(firstOperand, secondOperand, this.hashOperators.get(secondOperator)));
                operators.push(firstOperator);
            }
        }
    }

    private String getResult(String firstOperand, String secondOperand, ICalculator calcultor) {
        double op1 = Double.parseDouble(firstOperand);
        double op2 = Double.parseDouble(secondOperand);

        return String.valueOf(calcultor.calculate(op1, op2));
    }

    private void initializeHash() {
        this.hashOperators.put("+", (firstOperand, secondOperand) -> secondOperand + firstOperand);
        this.hashOperators.put("-", (firstOperand, secondOperand) -> secondOperand - firstOperand);
    }

    private String getValueCell(ReferenceCellParser referenceCellParser) throws InvalidFormulaException {
        Spreadsheet spreadsheet = Spreadsheet.getInstance();
        Book book = spreadsheet.getBook(referenceCellParser.getBookName());
        Sheet sheet = book.getSheet(referenceCellParser.getSheetName());
        String value = null;
        try {
            value = sheet.getCell(referenceCellParser.getRow(), referenceCellParser.getColumn()).getValue();
        } catch (Exception ex) {
            throw new InvalidFormulaException(Constant.INVALID_FORMULA_MSG);
        }
        if (value.isEmpty()) {
            throw new InvalidFormulaException(Constant.INVALID_FORMULA_MSG);
        }
        return value;
    }

    private void formulaValidate(String expression) throws InvalidFormulaException {
        if (!this.formulaValidator.validate(expression)) {
            throw new InvalidFormulaException(Constant.INVALID_FORMULA_MSG);
        }
    }

}
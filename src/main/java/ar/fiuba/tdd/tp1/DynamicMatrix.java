package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exception.InvalidFormulaException;

import java.util.ArrayList;

public class DynamicMatrix {

    private ArrayList<ArrayList<Cell>> matrix;

    public DynamicMatrix() {
        matrix = new ArrayList<ArrayList<Cell>>();
    }

    public ArrayList<ArrayList<Cell>> getMatrix() {
        return matrix;
    }

    public void setMatrix(ArrayList<ArrayList<Cell>> matrix) {
        this.matrix = matrix;
    }

    public void setValue(Integer column, Integer row, String value) {
        this.matrixIterator(column, row).setValue(value);
    }

    public Cell getCell(Integer column, Integer row) {
        if (!matrix.isEmpty() && matrix.get(column) != null && !matrix.get(column).isEmpty()
                && matrix.get(column).get(row) != null) {
            return matrix.get(column).get(row);

        } else {
            return null;
        }
    }

    public void setValueFormula(Integer column, Integer row, String formula, String bookName) throws InvalidFormulaException {
        this.matrixIterator(column, row).formula(formula,bookName);
    }

    private Cell matrixIterator(Integer column, Integer row) {
        if (matrix.size() > column) {
            if (matrix.get(column).size() > row) {
                matrix.get(column).set(row, new Cell());
            } else {
                fillRowsUpTo(column, row);
            }
        } else {
            fillColumnsUpTo(column, row);
        }
        return matrix.get(column).get(row);
    }
    
    private void fillRowsUpTo(Integer column, Integer row) {
        Integer indexRow = matrix.get(column).size();
        while (indexRow <= row) {
            if (matrix.get(column).isEmpty() || matrix.get(column).size() <= indexRow) {
                matrix.get(column).add(indexRow, new Cell());
            }
            indexRow++;
        }
    }
    
    private void fillColumnsUpTo(Integer column, Integer row) {
        Integer indexColumn = matrix.size();
        while (indexColumn <= column) {
            if (matrix.isEmpty() || matrix.size() <= indexColumn) {
                matrix.add(indexColumn, new ArrayList<Cell>());
                fillRowsUpTo(indexColumn, row);
            }
            indexColumn++;
        }
    }
    
}

package ar.fiuba.tdd.tp1;


import ar.fiuba.tdd.tp1.util.CreateList;

import java.util.ArrayList;

/**
 * Singleton de una hoja de calculo, almacena Hojas.
 *
 */
public class Spreadsheet {

    private static Spreadsheet instance;
    private ArrayList<Book> books;

    private Spreadsheet() {
        this.books = new ArrayList<Book>();
    }

    public static Spreadsheet getInstance() {
        if (instance == null) {
            instance = new Spreadsheet();
        }
        return instance;
    }
    
    public void addBook(Book book) {
        this.books.add(book);
    }

    public Book getBook(String bookName) {
        for (Book book : books) {
            if (bookName.equals(book.getName())) {
                return book;
            }
        }
        return null;
    }
    
    public boolean hasBook(String name) {
        if (this.getBook(name) == null) {
            return false;
        } else {
            return true;
        }
    }
    
    public ArrayList<String> getBookNames() {
        return CreateList.createListSinceProperty(new ArrayList<Object>(books), "name");
    }
    
    public void reset() {
        this.books.clear();
    }

}

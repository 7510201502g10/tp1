package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.Book;
import ar.fiuba.tdd.tp1.Sheet;

public class InsertSheetOnBookCommand implements Command {

    protected Book book;
    protected Sheet sheet;
    
    public InsertSheetOnBookCommand(Book oneBook, Sheet oneSheet) {
        this.sheet = oneSheet;
        this.book = oneBook;
    }
    
    public void execute() {
        this.sheet.setBookName(book.getName());
        this.book.getSheets().add(sheet);
    }

    public void undo() {
        this.book.getSheets().remove(sheet);
    }

}

package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.Sheet;

public class InsertOnCellCommand implements Command {

    protected Sheet sheet;
    protected String previousValue = "";
    protected int row;
    protected int column;
    protected String bookName;
    protected String value;

    public InsertOnCellCommand(Sheet oneSheet, int oneRow, int oneColumn,String value,String bookName) {
        this.sheet = oneSheet;
        this.row = oneRow;
        this.column = oneColumn;
        this.bookName = bookName;
        this.value = value;
        if (!sheet.getData().getMatrix().isEmpty() && sheet.getData().getMatrix().size() > column
                && !sheet.getData().getMatrix().get(column).isEmpty()
                        && sheet.getData().getMatrix().get(column).size() > row) {

            this.previousValue = oneSheet.getCell(row, column).getValue();
        } else {
            this.previousValue = null;
        }
    }

    public void execute() throws Exception {
    }

    @Override
    public void undo() {
        sheet.getCell(row, column).setValue(previousValue);
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}

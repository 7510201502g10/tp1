package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.Sheet;
import ar.fiuba.tdd.tp1.exception.BadFormulaException;
import ar.fiuba.tdd.tp1.exception.InvalidFormulaException;

public class InsertFormulaOnCellCommand extends InsertOnCellCommand {

    public InsertFormulaOnCellCommand(Sheet oneSheet, int oneRow, int oneColumn, String value, String bookName) {
        super(oneSheet, oneRow, oneColumn, value, bookName);
    }

    public void execute() throws Exception {
        try {
            sheet.getData().setValueFormula(column, row, value,bookName);
        } catch (InvalidFormulaException e) {
            throw new BadFormulaException("Error en formula");
        }
    }

}

package ar.fiuba.tdd.tp1.command;

import ar.fiuba.tdd.tp1.Sheet;

public class InsertValueOnCellCommand extends InsertOnCellCommand {

    public InsertValueOnCellCommand(Sheet oneSheet, int oneRow, int oneColumn, String value, String bookName) {
        super(oneSheet, oneRow, oneColumn, value, bookName);
    }

    public void execute() {
        sheet.getData().setValue(column, row, value);
    }

    @Override
    public void undo() {
        sheet.getCell(row, column).setValue(previousValue);
    }

}

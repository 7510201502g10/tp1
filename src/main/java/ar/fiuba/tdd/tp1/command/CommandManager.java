package ar.fiuba.tdd.tp1.command;

import java.util.Stack;
/*
 * Encargada de ejecutar comandos, mantener registro de comandos
 * para deshacerlos y/o rehacerlos.
 */
public class CommandManager {

    private Stack<Command> undos;
    private Stack<Command> redos;
    
    public CommandManager() {
        undos = new Stack<Command>();
        redos = new Stack<Command>();
    }
 
    public void executeCommand(Command command) throws Exception {
        try {
            command.execute();
        } catch (Exception e) {
            throw e;
        }
        undos.push(command);
        redos.clear();
    }
    
    public void undo() {
        if (!undos.empty()) {
            Command command = undos.pop();
            command.undo();
            redos.push(command);
        }
    }
    
    public void redo() throws Exception {
        if (!redos.empty()) {
            Command command = redos.pop();
            try {
                command.execute();
            } catch (Exception e) {
                throw e;
            }
            undos.push(command);
        }
    }

}
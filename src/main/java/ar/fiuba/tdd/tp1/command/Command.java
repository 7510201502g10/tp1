package ar.fiuba.tdd.tp1.command;

public interface Command {

    public abstract void execute() throws Exception;

    public void undo();

}

package ar.fiuba.tdd.tp1.util;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class CreateList {

    public static ArrayList<String> createListSinceProperty(ArrayList<Object> list, String property) {
        ArrayList<String> listOut = new ArrayList<String>();

        for (Object object : list) {
            Field field = null;
            String propertyOut = null;
            try {
                field = object.getClass().getDeclaredField(property);
                field.setAccessible(true);
                propertyOut = (String) field.get(object);
            } catch (NoSuchFieldException | SecurityException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }
            listOut.add(propertyOut);
        }
        return listOut;
    }

}

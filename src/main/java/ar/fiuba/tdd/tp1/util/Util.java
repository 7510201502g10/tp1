package ar.fiuba.tdd.tp1.util;

public class Util {
    
    public boolean isNumeric(String str) {
        return str.matches("^-?[0-9]+(\\.[0-9]+)?$");
    }
    
    public boolean isCell(String str) {
        return str.matches("^[A-Z]{1,100}[0-9]{1,100}!([aA-zZ]|[0-9]){1,100}$");
    }

}

package ar.fiuba.tdd.tp1.operation;

@FunctionalInterface
public interface ICalculator {

    public Double calculate(Double firstOperand, Double secondOperand);
}

package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.command.CommandManager;
import ar.fiuba.tdd.tp1.command.InsertFormulaOnCellCommand;
import ar.fiuba.tdd.tp1.command.InsertValueOnCellCommand;

public class Sheet {

    private DynamicMatrix data;
    private String name;
    private CommandManager commandManager;
    private String bookName;

    public Sheet(String name) {
        this.data = new DynamicMatrix();
        this.name = name;
        this.commandManager = new CommandManager();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void addValueFormula(Integer row, Integer column, String value) throws Exception {
        commandManager.executeCommand(new InsertFormulaOnCellCommand(this, row, column, value,bookName));
    }
    
    public void addValue(Integer row, Integer column, String value) throws Exception {
        commandManager.executeCommand(new InsertValueOnCellCommand(this, row, column, value, bookName));
    }
    
    public void insertFormulaOnCell(Integer row, Integer column, String formula) throws Exception {
        commandManager.executeCommand(new InsertFormulaOnCellCommand(this, row, column, formula, bookName));
    }

    public String getCellValue(Integer row, Integer column) {
        return (String) data.getMatrix().get(column).get(row).getValue();
    }
    
    public String getCellFormula(Integer row, Integer column) {
        return (String) data.getMatrix().get(column).get(row).getFormula();
    }

    public Cell getCell(Integer row, Integer column) {
        return (Cell) data.getMatrix().get(column).get(row);
    }
    
    public void undo() {
        commandManager.undo();
    }
    
    public void redo() throws Exception {
        try {
            commandManager.redo();
        } catch (Exception e) {
            throw e;
        }
    }

    public DynamicMatrix getData() {
        return data;
    }

    public void setData(DynamicMatrix data) {
        this.data = data;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

   
}

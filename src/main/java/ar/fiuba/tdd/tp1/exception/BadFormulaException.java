package ar.fiuba.tdd.tp1.exception;

public class BadFormulaException extends Exception {

    private static final long serialVersionUID = 2220253948360451503L;

    public BadFormulaException(String msg) {
        super(msg);
    }
}

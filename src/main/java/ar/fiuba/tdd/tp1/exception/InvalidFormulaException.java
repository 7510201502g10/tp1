package ar.fiuba.tdd.tp1.exception;

/**
 * Excepcion creada para indicar que una formula es invalida.
 * @author lpennese
 *
 */
public class InvalidFormulaException extends Exception {

    private static final long serialVersionUID = 4123900813823869452L;

    public InvalidFormulaException(String msg) {
        super(msg);
    }

}

package ar.fiuba.tdd.tp1.exception;

public class UndeclaredWorkSheetException extends Exception {

    private static final long serialVersionUID = 6333717294358640165L;

    public UndeclaredWorkSheetException(String msg) {
        super(msg);
    }
}

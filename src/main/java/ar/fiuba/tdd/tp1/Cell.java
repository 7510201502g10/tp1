package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exception.InvalidFormulaException;
import ar.fiuba.tdd.tp1.operation.ICalculator;

/** 
 * Clase que representa una celda, encargada de almacenar valores.
 *
 */
public class Cell {

    private String value;
    private String formula;
    private ICalculator calculator;
    private Cell reference = null;

    public Cell(String value) {
        super();
        this.value = value;
        this.reference = null;
    }

    public Cell() {
        super();
        this.value = "";
    }
    
    public String getFormula() {
        return formula;
    }
    
    public void setFormula(String formula) {
        this.formula = formula;
    }
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ICalculator getCalculator() {
        return calculator;
    }

    public void setCalculator(ICalculator calculator) {
        this.calculator = calculator;
    }

    public Cell getReference() {
        return reference;
    }

    public void setReference(Cell reference) {
        this.reference = reference;
        this.value = reference.getValue();
    }

    public void sumCell(Cell cell) {
        try {
            this.value = Double
                    .toString(this.sum(Double.parseDouble(this.getValue()), Double.parseDouble(cell.getValue())));
        } catch (NumberFormatException ex) {
            throw new NumberFormatException();
        }
    }

    public void sumNumber(Double number) {
        this.value = Double.toString(this.sum(Double.parseDouble(this.getValue()), number));
    }

    public void substractCell(Cell cell) {
        try {
            this.value = Double
                    .toString(this.substract(Double.parseDouble(this.getValue()), Double.parseDouble(cell.getValue())));
        } catch (NumberFormatException ex) {
            throw new NumberFormatException();
        }
    }

    public void substractNumber(Double number) {
        this.value = Double.toString(this.substract(Double.parseDouble(this.getValue()), number));
    }

    private Double sum(Double firstOperand, Double secondOperand) {
        this.calculator = (first, second) -> second + first;
        return calculator.calculate(firstOperand, secondOperand);
    }

    private Double substract(Double firstOperand, Double secondOperand) {
        this.calculator = (first, second) -> first - second;
        return calculator.calculate(firstOperand, secondOperand);
    }
    
    public void formula(String expression,String bookName) throws InvalidFormulaException {
        InfixEvaluator infixEvaluator = new InfixEvaluator();
        this.setValue(infixEvaluator.infix(expression,bookName));
    }
}

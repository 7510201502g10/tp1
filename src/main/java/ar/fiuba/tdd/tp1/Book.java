package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.command.CommandManager;
import ar.fiuba.tdd.tp1.command.InsertSheetOnBookCommand;
import ar.fiuba.tdd.tp1.util.CreateList;

import java.util.ArrayList;

public class Book {
    
    private String name;
    private ArrayList<Sheet> sheets;
    private CommandManager commandManager;
    
    public Book(String name) {
        this.name = name;
        this.sheets = new ArrayList<Sheet>();
        this.commandManager = new CommandManager();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void addSheet(Sheet sheet) throws Exception {
        commandManager.executeCommand(new InsertSheetOnBookCommand(this, sheet));
    }
    
    public ArrayList<Sheet> getSheets() {
        return this.sheets;
    }

    public Sheet getSheet(String sheetName) {
        for (Sheet sheet : sheets) {
            if (sheetName.equals(sheet.getName())) {
                return sheet;
            }
        }
        return null;
    }
    
    public ArrayList<String> getSheetNames() {
        return CreateList.createListSinceProperty(new ArrayList<Object>(sheets), "name");
    }
    
    public void undo() {
        commandManager.undo();
    }
    
    public void redo() throws Exception {
        try {
            commandManager.redo();
        } catch (Exception e) {
            throw e;
        }
    }

}

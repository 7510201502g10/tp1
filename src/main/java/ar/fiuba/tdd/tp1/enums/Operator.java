package ar.fiuba.tdd.tp1.enums;

public enum Operator {

    SUM("+", 2), SUBSTRACT("-", 2), EQUAL("=", 3);

    private String operator;
    private Integer priority;

    private Operator(String operator, Integer priority) {
        this.operator = operator;
        this.priority = priority;
    }

    public String getOperator() {
        return operator;
    }

    private Integer getPriority() {
        return priority;
    }

    public static Integer getPriorityFromOperator(String opertador) {

        Integer resultPriority = null;
        for (Operator priority : Operator.values()) {
            if (opertador.equals(priority.getOperator())) {
                resultPriority = priority.getPriority();
            }
        }
        return resultPriority;

    }

    public static Boolean isOperator(String operator) {
        for (Operator operatorEnum : Operator.values()) {
            if (operator.equals(operatorEnum.getOperator())) {
                return true;
            }
        }
        return false;
    }

}

package ar.fiuba.tdd.tp1;

public class Constant {

    public static final String EQUAL = "=";
    public static final String SPACE = " ";
    public static final String SEPARATOR = "!";
    
    public static final String INVALID_FORMULA_MSG = "Fórmula Inválida.";

}

package ar.fiuba.tdd.tp1.parser;

import ar.fiuba.tdd.tp1.Column;
import ar.fiuba.tdd.tp1.Constant;
import ar.fiuba.tdd.tp1.util.Util;

/**
 * Clase encargada de parsear una referencia de celda Ej: A1!hoja1.
 *
 */
public class ReferenceCellParser {

    private Integer row;
    private Integer column;
    private String sheetName;
    private String bookName;

    public ReferenceCellParser(String bookName) {
        super();
        this.bookName = bookName;
    }


    public Integer getRow() {
        return row;
    }

    public Integer getColumn() {
        return column;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public void parserReferenceCell(String referenceCellName) {
        String rowColumn = referenceCellName.substring(0, referenceCellName.indexOf(Constant.SEPARATOR));
        this.sheetName = referenceCellName.substring(referenceCellName.indexOf(Constant.SEPARATOR) + 1);
        StringBuffer row = new StringBuffer();
        StringBuffer column = new StringBuffer();
        char[] chars = rowColumn.toCharArray();
        for (int i = 0, n = chars.length; i < n; i++) {
            char character = chars[i];
            if (new Util().isNumeric(String.valueOf(character))) {
                row.append(String.valueOf(character));
            } else {
                column.append(String.valueOf(character));
            }
        }
        this.column = new Column().getPositionFromNameColumn(column.toString());
        this.row = Integer.valueOf(row.toString());
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }
}

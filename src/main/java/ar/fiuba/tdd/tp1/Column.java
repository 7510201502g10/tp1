package ar.fiuba.tdd.tp1;

import java.util.HashMap;
import java.util.Map;

public class Column {

    private final Map<String, Integer> map;

    public Column() {
        map = new HashMap<>();
        map.put("A", 1); 
        map.put("B", 2);
        map.put("C", 3);
        map.put("D", 4);
        map.put("E", 5);
        map.put("F", 6);
        map.put("G", 7);
        map.put("H", 8);
        map.put("I", 9);
        map.put("J", 10);
        map.put("K", 11);
        map.put("L", 12);
        map.put("M", 13);
        map.put("N", 14);
        map.put("O", 15);
        map.put("P", 16);
        map.put("Q", 17);
        map.put("R", 18);
    }

    public Integer getPositionFromNameColumn(String nameColumn) {
        Integer position = null;
        position = (Integer) map.get(nameColumn);
        return position;
    }
}

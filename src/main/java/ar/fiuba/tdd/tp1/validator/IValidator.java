package ar.fiuba.tdd.tp1.validator;

public interface IValidator {
    
    public Boolean validate(Object object);

}

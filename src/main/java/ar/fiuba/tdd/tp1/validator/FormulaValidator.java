package ar.fiuba.tdd.tp1.validator;

import ar.fiuba.tdd.tp1.Constant;
import ar.fiuba.tdd.tp1.enums.Operator;


import ar.fiuba.tdd.tp1.util.Util;

/**
 * Encargada de validar la formula insertada en una celda.
 *
 */
public class FormulaValidator implements IValidator {

    @Override
    public Boolean validate(Object object) {
        String formula = (String) object;
        
        Util numberUtil = new Util();

        String previous = "";

        for (String token : formula.trim().split(Constant.SPACE)) {
            if (previous.isEmpty()) {
//                if (!numberUtil.isNumeric(token) && !numberUtil.isCell(token)) {
//                    return false;
//                }
                if (!token.equals(Constant.EQUAL)) {
                    return false;
                }
                previous = token;

            } else if (numberUtil.isNumeric(previous) || numberUtil.isCell(previous)) {
                if (!Operator.isOperator(token)) {
                    return false;
                }
                previous = token;

            } else {
                if (!numberUtil.isNumeric(token) && !numberUtil.isCell(token)) {
                    return false;
                }
                previous = token;
            }
        }
        return this.evaluateLastToken(previous, numberUtil);
    }

    private Boolean evaluateLastToken(String token, Util numberUtil) {
        if (numberUtil.isNumeric(token) || numberUtil.isCell(token)) {
            return true;
        }
        return false;
    }

}

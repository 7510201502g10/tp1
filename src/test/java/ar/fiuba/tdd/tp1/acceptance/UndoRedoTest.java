package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;

import org.hamcrest.core.IsCollectionContaining;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;





public class UndoRedoTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadSheetTestDriver();
    }

    private void commonHistory() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "10");
        testDriver.setCellValue("tecnicas", "default", "A1", "5");
    }

    @Test
    public void undoValue() throws Exception {
        commonHistory();

        testDriver.undo("", "");
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void redoValue() throws Exception {
        commonHistory();

        testDriver.redo("", "");
        assertEquals(5, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void undoThenRedoValue() throws Exception {
        commonHistory();

        testDriver.undo("", "");
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);

        testDriver.redo("", "");
        assertEquals(5, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void actionClearsRedo() throws Exception {
        commonHistory();

        testDriver.undo("", "");
        testDriver.setCellValue("tecnicas", "default", "A1", "7");
        testDriver.redo("", "");

        assertEquals(7, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void undoSheetCreation() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "sheet 1");
        testDriver.createNewWorkSheetNamed("tecnicas", "sheet 2");

        testDriver.bookUndo("");

        assertThat(testDriver.workSheetNamesFor("tecnicas"), IsCollectionContaining.hasItem("sheet 1"));
        assertThat(testDriver.workSheetNamesFor("tecnicas"), not(IsCollectionContaining.hasItem("sheet 2")));
    }

    @Test
    public void redoSheetCreation() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "sheet 1");
        testDriver.createNewWorkSheetNamed("tecnicas", "sheet 2");

        testDriver.bookUndo("");
        assertThat(testDriver.workSheetNamesFor("tecnicas"), IsCollectionContaining.hasItem("sheet 1"));
        assertThat(testDriver.workSheetNamesFor("tecnicas"), not(IsCollectionContaining.hasItem("sheet 2")));

        testDriver.bookRedo("");
        assertThat(testDriver.workSheetNamesFor("tecnicas"), IsCollectionContaining.hasItem("sheet 1"));
        assertThat(testDriver.workSheetNamesFor("tecnicas"), IsCollectionContaining.hasItem("sheet 2"));
    }
}
package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;

import org.hamcrest.core.IsCollectionContaining;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class CreationTest {

    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadSheetTestDriver();
    }

    @Test
    public void startWithNoWorkBooks() {
        assertTrue(testDriver.workBooksNames().isEmpty());
    }

    @Test
    public void createOneWorkBook() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");

        assertThat(testDriver.workBooksNames(), IsCollectionContaining.hasItem("tecnicas"));
    }

    @Test
    public void createMultipleWorkBooks() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas 1");
        testDriver.createNewWorkBookNamed("tecnicas 2");

        assertThat(testDriver.workBooksNames(), IsCollectionContaining.hasItems("tecnicas 1", "tecnicas 2"));
        assertThat(testDriver.workBooksNames(), not(IsCollectionContaining.hasItem("tecnicas")));
    }

    @Test
    public void workBookStartsWithDefaultWorkSheet() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        

        assertThat(testDriver.workSheetNamesFor("tecnicas"), IsCollectionContaining.hasItem("default"));
    }

    @Test
    public void createOneWorkSheet() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "other");

        assertThat(testDriver.workSheetNamesFor("tecnicas"), IsCollectionContaining.hasItem("other"));
    }

    @Test
    public void createAdditionalWorkSheets() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "firstAdditionalWorksheet");
        testDriver.createNewWorkSheetNamed("tecnicas", "secondAdditionalWorksheet");

        assertThat(testDriver.workSheetNamesFor("tecnicas"), IsCollectionContaining.hasItem("firstAdditionalWorksheet"));
        assertThat(testDriver.workSheetNamesFor("tecnicas"), IsCollectionContaining.hasItem("secondAdditionalWorksheet"));
    }

}
package ar.fiuba.tdd.tp1.acceptance.driver;

import ar.fiuba.tdd.tp1.Book;
import ar.fiuba.tdd.tp1.Column;
import ar.fiuba.tdd.tp1.Sheet;
import ar.fiuba.tdd.tp1.Spreadsheet;

import java.util.ArrayList;

public class SpreadSheetTestDriver {
    
    private Spreadsheet spreadsheet;
    
    public SpreadSheetTestDriver() {
        spreadsheet = Spreadsheet.getInstance();
        spreadsheet.reset();
    }
    
    public void createNewWorkBookNamed(String name) throws Exception {
        spreadsheet.addBook(new Book(name));
        spreadsheet.getBook(name).addSheet(new Sheet("default"));
    }
    
    public void createNewWorkSheetNamed(String bookName, String sheetName) throws Exception {
        spreadsheet.addBook(new Book(bookName));
        spreadsheet.getBook(bookName).addSheet(new Sheet(sheetName));
    }
    
    public ArrayList<String> workBooksNames() {
        return spreadsheet.getBookNames();
    }
    
    public void setCellValue(String bookName, String sheetName, String cell, String value) throws Exception {
        Integer row = this.getRow(cell);
        Integer column = this.getColumn(cell);
        spreadsheet.getBook(bookName).getSheet(sheetName).addValue(row, column, value);
    }
    
    public void setCellFormula(String bookName, String sheetName, String cell, String value) throws Exception {
        Integer row = this.getRow(cell);
        Integer column = this.getColumn(cell);
        spreadsheet.getBook(bookName).getSheet(sheetName).addValueFormula(row, column, value);
    }
    
    public String getCellValueAsString(String bookName, String sheetName, String cell) {
        Integer row = this.getRow(cell);
        Integer column = this.getColumn(cell);
        
        return spreadsheet.getBook(bookName).getSheet(sheetName).getCellValue(row, column);
    }
    
    public Double getCellValueAsDouble(String bookName, String sheetName, String cell) {
        Integer row = this.getRow(cell);
        Integer column = this.getColumn(cell);
        
        return Double.valueOf(spreadsheet.getBook(bookName).getSheet(sheetName).getCellValue(row, column));
    }
    
    public void undo(String bookName, String sheetName) {
        if (bookName.equals("")) {
            bookName = spreadsheet.getBookNames().get(0);
        }
        if (sheetName.equals("")) {
            sheetName = spreadsheet.getBook(bookName).getSheetNames().get(0);
        }
        
        spreadsheet.getBook(bookName).getSheet(sheetName).undo();
    }
    
    public void redo(String bookName, String sheetName) throws Exception {
        if (bookName.equals("")) {
            bookName = spreadsheet.getBookNames().get(0);
        }
        if (sheetName.equals("")) {
            sheetName = spreadsheet.getBook(bookName).getSheetNames().get(0);
        }
        
        spreadsheet.getBook(bookName).getSheet(sheetName).redo();
    }
    
    public void bookUndo(String bookName) {
        if (bookName.equals("")) {
            bookName = spreadsheet.getBookNames().get(0);
        }
        
        spreadsheet.getBook(bookName).undo();
    }
    
    public void bookRedo(String bookName) throws Exception {
        if (bookName.equals("")) {
            bookName = spreadsheet.getBookNames().get(0);
        }
        
        spreadsheet.getBook(bookName).redo();
    }
    
    public ArrayList<String> workSheetNamesFor(String bookName) {
        return spreadsheet.getBook(bookName).getSheetNames();
    }

    private Integer getColumn(String cell) {
        String column = cell.substring(0, 1);
        return new Column().getPositionFromNameColumn(column);
    }
    
    private Integer getRow(String cell) {
        return Integer.valueOf(cell.substring(1));
    }
}

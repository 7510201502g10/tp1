package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SimpleValuesTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadSheetTestDriver();
    }

    @Test
    public void setAndRetrieveStringValue() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", "Hello World!");

        assertEquals("Hello World!", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void setAndRetrieveNumberValue() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", "1000.50");

        assertEquals(1000.50, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void setAndRetrieveMultipleNumberValues() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", "1000.50");
        testDriver.setCellValue("tecnicas", "default", "A2", "10");
        testDriver.setCellValue("tecnicas", "default", "A3", "1");
        testDriver.setCellValue("tecnicas", "default", "A4", "-10.50");
        testDriver.setCellValue("tecnicas", "default", "A5", "0.0");

        assertEquals(1000.50, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
        assertEquals(10, testDriver.getCellValueAsDouble("tecnicas", "default", "A2"), DELTA);
        assertEquals(1, testDriver.getCellValueAsDouble("tecnicas", "default", "A3"), DELTA);
        assertEquals(-10.50, testDriver.getCellValueAsDouble("tecnicas", "default", "A4"), DELTA);
        assertEquals(0.0, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    @Test
    public void updateCellValue() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.setCellValue("tecnicas", "default", "A1", "1000.50");
        assertEquals(1000.50, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);

        testDriver.setCellValue("tecnicas", "default", "A1", "Hello");
        assertEquals("Hello", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }


}
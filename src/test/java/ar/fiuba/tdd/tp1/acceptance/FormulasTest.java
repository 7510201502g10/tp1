package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.exception.BadFormulaException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class FormulasTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new SpreadSheetTestDriver();
    }

    @Test
    public void sumLiterals() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellFormula("tecnicas", "default", "A1", "= 1 + 0 + 3.5 + -1");

        assertEquals(3.5, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void sumLiteralsInDifferentRows() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "1");
        testDriver.setCellValue("tecnicas", "default", "B2", "2");
        testDriver.setCellFormula("tecnicas", "default", "C3", "= A1!default + B2!default");
        
        assertEquals(1 + 2, testDriver.getCellValueAsDouble("tecnicas", "default", "C3"), DELTA);
    }

    @Test
    public void subtractLiterals() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellFormula("tecnicas", "default", "A1", "= 1 - 0 - 3.5 - -1");

        assertEquals(1 - 3.5 - -1, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void formulaWithReferences() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        
        testDriver.setCellValue("tecnicas", "default", "A2", "5");
        testDriver.setCellValue("tecnicas", "default", "A3", "2");
        testDriver.setCellFormula("tecnicas", "default", "A1", "= 1 + A2!default + 0.5 - A3!default");

        assertEquals(1 + 5 + 0.5 - 2, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void formulaWithReferenceToReference() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        
        testDriver.setCellValue("tecnicas", "default", "A3", "3");
        testDriver.setCellFormula("tecnicas", "default", "A2", "= 2 + A3!default");
        testDriver.setCellFormula("tecnicas", "default", "A1", "= 1 + A2!default");

        assertEquals(1 + 2 + 3, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }

    @Test
    public void formulaWithReferencesFromOtherSpreadSheet() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "other");
        
        testDriver.setCellValue("tecnicas", "other", "A2", "-1");
        testDriver.setCellValue("tecnicas", "default", "A3", "2");
        testDriver.setCellFormula("tecnicas", "default", "A1", "= 1 + A2!other + 0.5 - A3!default");

        assertEquals(1 + -1 + 0.5 - 2, testDriver.getCellValueAsDouble("tecnicas", "default", "A1"), DELTA);
    }


    @Test(expected = BadFormulaException.class)
    public void badFormulaStringAndNumber() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellFormula("tecnicas", "default", "A1", "= 1 + hello");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

/*    @Test
    public void badFormulaStringAndNumberRetrieveAsString() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellFormula("tecnicas", "default", "A1", "= 1 + hello");

        testDriver.getCellValueAsString("tecnicas", "default", "Error:BAD_FORMULA");
    }
*/
    @Test(expected = BadFormulaException.class)
    public void badFormulaWithReference() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A2", "Hello");
        testDriver.setCellFormula("tecnicas", "default", "A1", "= 1 + A2");
        
        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    @Test(expected = BadFormulaException.class)
    public void badFormulaWithoutEqualSymbol() throws Exception {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellFormula("tecnicas", "default", "A1", "1 + 2");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

/*    @Ignore("Optional expected behavior test")
    @Test(expected = UndeclaredWorkSheetException.class)
    public void undeclaredWorkSheetInvocation() {
        testDriver.createNewWorkBookNamed("tecnicas");

        testDriver.getCellValueAsString("tecnicas", "undeclaredWorkSheet", "A1");
    }
*/
}
package ar.fiuba.tdd.tp1;

import ar.fiuba.tdd.tp1.exception.InvalidFormulaException;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class InfixTest {

    public Spreadsheet spreadsheet = null;

    @Before
    public void setUp() {
        spreadsheet = Spreadsheet.getInstance();
    }

    @Test
    public void infixWithCellTest() throws Exception {
        Sheet sheet = new Sheet("hoja12");
        Book book = new Book("book1");
        book.addSheet(sheet);
        sheet.addValue(1, 1, "5");
        spreadsheet.addBook(book);

        Cell cell = new Cell();
        cell.formula("= 2 + 3 + A1!hoja12 - 6 + 10","book1");
        assertTrue(14 == Double.parseDouble(cell.getValue()));
    }

    @Test
    public void infixFormulaTwoCellOperand() throws Exception {
        Sheet sheet = new Sheet("hoja4");
        Book book = new Book("book2");
        sheet.addValue(1, 2, "16");
        sheet.addValue(4, 3, "12");
        book.addSheet(sheet);
        spreadsheet.addBook(book);
        Cell cell = new Cell();
        
        cell.formula("= 2 + 3 + B1!hoja4 - 6 + 10 + C4!hoja4","book2");
        assertEquals("37.0", cell.getValue());
    }
    
    @Test
    public void infixFormulaWithTwoSheet() throws Exception {
        Sheet sheet = new Sheet("hoja1232");
        Sheet otherSheet = new Sheet("OtraHoja2");
        Book book = new Book("book3");
        sheet.addValue(1, 3, "16");
        otherSheet.addValue(4, 3, "12");
        book.addSheet(sheet);
        book.addSheet(otherSheet);
        spreadsheet.addBook(book);
        
        Cell cell = new Cell();
        cell.formula("= 2 + 3 + C1!hoja1232 - 6 + 10 + C4!OtraHoja2","book3");
        assertEquals("37.0", cell.getValue());
    }
    
    @Test
    public void infixSubstract() throws Exception {
        Sheet sheet = new Sheet("hojaSubstrac");
        Book book = new Book("book4");
        sheet.addValue(1, 1, "5");
        book.addSheet(sheet);
        spreadsheet.addBook(book);
        Cell cell = new Cell();

        cell.formula("= 2 - 6 - 10 - 324","book4");
        assertTrue(-338 == Double.parseDouble(cell.getValue()));
    }
    
    @Test
    public void infixSubstractWithCell() throws Exception {
        Sheet sheet = new Sheet("hojaSubstracWithCell");
        Book book = new Book("book5");
        sheet.addValue(4, 5, "5");
        book.addSheet(sheet);
        spreadsheet.addBook(book);

        Cell cell = new Cell();
        cell.formula("= 2 - 6 - 10 - E4!hojaSubstracWithCell","book5");
        assertTrue(-19 == Double.parseDouble(cell.getValue()));
    }
    
    @Test
    public void infixSubstractWithNegativeNumber() throws Exception {
        Sheet sheet = new Sheet("hojaSubstrac");
        Book book = new Book("book6");
        sheet.addValue(1, 1, "5");
        book.addSheet(sheet);
        spreadsheet.addBook(book);

        Cell cell = new Cell();
        cell.formula("= -2 - -6 - -10 - -324","book6");
        assertTrue(338 == Double.parseDouble(cell.getValue()));
    }

    @Test(expected = InvalidFormulaException.class)
    public void infixFormulaInvalidEmptyCell() throws Exception {
        Sheet sheet = new Sheet( "hoja1");
        Book book = new Book("book7");
        sheet.addValue(1, 2, "16");
        sheet.addValue(4, 3, "12");
        book.addSheet(sheet);
        spreadsheet.addBook(book);
        
        Cell cell = new Cell();
        cell.formula("= 2 + 3 + A1!hoja1 - 6 + 10 + C4!hoja1","book7");
    }
    
    
    @Test(expected = InvalidFormulaException.class)
    public void infixFormulaInvalidOperand() throws Exception {
        Sheet sheet = new Sheet("unaHoja");
        Book book = new Book("book8");
        sheet.addValue(1, 2, "16");
        sheet.addValue(4, 3, "12");
        book.addSheet(sheet);
        spreadsheet.addBook(book);
        
        Cell cell = new Cell();
        cell.formula("= 2 + 3 !! A1!unaHoja - 6 + 10 + C4!unaHoja","book8");
    }
    
    @Test(expected = InvalidFormulaException.class)
    public void infixInvalidCell() throws Exception {
        Sheet sheet = new Sheet("hoja1");
        Book book = new Book("book9");
        sheet.addValue(1, 1, "5");
        book.addSheet(sheet);
        spreadsheet.addBook(book);

        Cell cell = new Cell();
        cell.formula("= 2 + 3 + AB12344!hoja1 - 6 + 10","book9");
    }
    
    @Test(expected = InvalidFormulaException.class)
    public void infixInvalidOperand() throws Exception {
        Sheet sheet = new Sheet("hoja1");
        Book book = new Book("book10");
        sheet.addValue(1, 1, "5");
        book.addSheet(sheet);
        spreadsheet.addBook(book);

        Cell cell = new Cell();
        cell.formula("= 2 + 3 * 6 + 10","book10");
    }

}

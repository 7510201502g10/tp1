package ar.fiuba.tdd.tp1;


import ar.fiuba.tdd.tp1.validator.FormulaValidator;
import ar.fiuba.tdd.tp1.validator.IValidator;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class FormulaValidatorTest {

    @Test
    public void validFormula() {
        IValidator validator = new FormulaValidator();
        assertTrue(validator.validate("= 2 + 3 - 2 + 10"));
    }

    @Test
    public void validFormulaWithCell() {
        IValidator validator = new FormulaValidator();
        assertTrue(validator.validate("= 2 + A1!hoja1 - 2 + B22!hoja2 + 556565"));
    }

    @Test
    public void validFormulaEndsWhitCell() {
        IValidator validator = new FormulaValidator();
        assertTrue(validator.validate("= 2 + A1!hoja1 - 2 + 67 + B22!hoja2"));
    }

    @Test
    public void validFormulaOnlyNumber() {
        IValidator validator = new FormulaValidator();
        assertTrue(validator.validate("= 10"));
    }

    @Test
    public void validFormulaOnlyCell() {
        IValidator validator = new FormulaValidator();
        assertTrue(validator.validate("= A133!hoja1"));
    }

    @Test
    public void invalidFormula() {
        IValidator validator = new FormulaValidator();
        assertTrue(false == validator.validate("= + 2 + 3 - 2 + 10"));
    }

    @Test
    public void invalidFormulaEndsWhitOperand() {
        IValidator validator = new FormulaValidator();
        assertTrue(false == validator.validate("= 10 + 2 + 3 - 2 +"));
    }

    @Test
    public void invalidFormulaOnlyOperand() {
        IValidator validator = new FormulaValidator();
        assertTrue(false == validator.validate("= +"));
    }

    @Test
    public void invalidFormulaInvalidOperand() {
        IValidator validator = new FormulaValidator();
        assertTrue(false == validator.validate("= 10 +* 2 + 3"));
    }

    @Test
    public void invalidFormulaOnlyInvalidOperand() {
        IValidator validator = new FormulaValidator();
        assertTrue(false == validator.validate("= ++"));
    }

    @Test
    public void invalidFormulaInvalidFormatCell() {
        IValidator validator = new FormulaValidator();
        assertTrue(false == validator.validate("= 2 + A1hoja1 - 2 + 67"));
    }
    
    @Test
    public void invalidFormulaWithoutEquals() {
        IValidator validator = new FormulaValidator();
        assertTrue(false == validator.validate("2 + A1hoja1 - 2 + 67"));
    }
}

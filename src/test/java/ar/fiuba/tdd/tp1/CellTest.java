package ar.fiuba.tdd.tp1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CellTest {

    private static final double DELTA = 0.00001;

    @Test
    public void sumCellTest() {
        Cell firstCell = new Cell("22.5");
        Cell secondCell = new Cell("34.1");
        firstCell.sumCell(secondCell);

        assertEquals(22.5 + 34.1, Float.parseFloat(firstCell.getValue()), DELTA);
    }
    
    @Test
    public void sumNumberTest() {
        Cell firstCell = new Cell("22.5");
        Double secondOperand = new Double("34.1");
        firstCell.sumNumber(secondOperand);

        assertEquals(22.5 + 34.1, Double.parseDouble(firstCell.getValue()), DELTA);
    }
    
    @Test(expected = NumberFormatException.class)
    public void sumCellNumberFormarExceptionTest() {
        Cell firstCell = new Cell("22.5");
        Cell secondCell = new Cell("aaaaa");
        firstCell.sumCell(secondCell);
    }
    
    @Test
    public void substractCellTest() {
        Cell firstCell = new Cell("22.5");
        Cell secondCell = new Cell("34.1");
        firstCell.substractCell(secondCell);

        assertEquals(22.5 - 34.1, Double.parseDouble(firstCell.getValue()), DELTA);
    }
    
    @Test(expected = NumberFormatException.class)
    public void substractCellNumberFormarExceptionTest() {
        Cell firstCell = new Cell("22.5");
        Cell secondCell = new Cell("aaaaa");
        firstCell.substractCell(secondCell);
    }
    
    @Test
    public void substractNumberTest() {
        Cell firstCell = new Cell("22.5");
        Double secondOperand = new Double("34.1");
        firstCell.substractNumber(secondOperand);

        assertEquals(22.5 - 34.1, Double.parseDouble(firstCell.getValue()), DELTA);
    }
    
    @Test
    public void cellReference() {
        Cell firstCell = new Cell("22.5");
        Cell secondCell = new Cell("34.1");
        
        secondCell.setReference(firstCell);
        
        assertEquals(22.5, Double.parseDouble(secondCell.getValue()), DELTA);
    }
    
    @Test
    public void cellReferenceGetValue() {
        Cell firstCell = new Cell("22.5");
        Cell secondCell = new Cell("34.1");
        
        secondCell.setReference(firstCell);

        assertTrue(firstCell == secondCell.getReference());
    }
    
}

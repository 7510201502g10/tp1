package ar.fiuba.tdd.tp1;

import org.junit.Test;

import static org.junit.Assert.*;

public class CommandTest {

    @Test
    public void addValueCommandTest() throws Exception {
        Sheet sheet = new Sheet("Prueba");
        sheet.addValue(2, 2, "1");
        sheet.addValue(2, 2, "4");
        
        assertEquals("4", sheet.getCellValue(2, 2));
    }

    @Test
    public void addValueCommandAndUndoTest() throws Exception {
        Sheet sheet = new Sheet("Prueba");
        sheet.addValue(1, 1, "1");
        sheet.addValue(1, 1, "4");
        
        assertEquals("4", sheet.getCellValue(1, 1));
        
        sheet.undo();
        
        assertEquals("1", sheet.getCellValue(1, 1));
    }

    @Test
    public void undoAndRedoTest() throws Exception {
        Sheet sheet = new Sheet("Prueba");
        sheet.addValue(1, 1, "1");
        sheet.addValue(1, 1, "4");
        
        sheet.undo();
        sheet.redo();
        
        assertEquals("4", sheet.getCellValue(1, 1));
    }
    
    @Test
    public void insertFormulaOnCellCommandTest() throws Exception {
        Spreadsheet spreadSheet = Spreadsheet.getInstance();
        spreadSheet.reset();
        Sheet sheet = new Sheet("OtraHoja1");
        Book book = new Book("book");
        book.addSheet(sheet);
        spreadSheet.addBook(book);
        sheet.insertFormulaOnCell(1, 1, "= 1 + 2");
        assertEquals("3.0", sheet.getCellValue(1, 1));
        
        sheet.insertFormulaOnCell(2, 2, "= 1 + A1!OtraHoja1");
        assertEquals("4.0", sheet.getCellValue(2, 2));
    }
    
}
